#include "scene.hh"
#include <chrono>
#include <thread>
#include <fstream>

using namespace std::chrono;
using namespace std::this_thread;
using namespace std;

scene::scene() {

    link.Init();
    link.SetDrawingMode(PzG::DM_3D);

    link.SetRangeX(-400, 400);
    link.SetRangeY(-400, 400);
    link.SetRangeZ(-400, 400);

    link.AddFilename(bottomSurface.kBottomSurface.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(upperSurface.kUpperSurface.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(drone.kDroneFile.c_str(), PzG::LS_CONTINUOUS, 1);

    drone.draw(drone.kDroneFile);

    link.Draw();
}

void scene::Move(const double &MovingAngle, const double &lenght) {
    for (unsigned i = 0; i < FPS; ++i) {
        drone.TranslateForward(MovingAngle, lenght, FPS);
        link.Draw();
        sleep_for(microseconds(FREEZE));
    }
}

void scene::Rotation(double RotateAngle) {
    for (unsigned i = 0; i < FPS; ++i) {
        drone.Rotate(RotateAngle, FPS);
        link.Draw();
        sleep_for(microseconds(FREEZE));
    }
}
