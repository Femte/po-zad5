#include <iostream>
#include "scene.hh"
#include <fstream>

using namespace std;

int main() {
    scene scene;
    char choose = '-';
    while (choose != 'k') {
        cout << "r - zadaj ruch na wprost" << endl;
        cout << "o - zadaj zmiane orientacji" << endl;
        cout << "k - koniec dzialania programu" << endl;
        cin >> choose;

        switch (choose) {
            case 'r': {
                double angle = 0, distance = 0;
                cout << "Wybrales ruch drona" << endl;
                cout << "Podaj wartosc kata (wznoszenia/opadania) w stopniach" << endl << "Wartosc kata: ";
                cin >> angle;
                cout << "Podaj odleglosc przemieszczenia drona" << endl << "Dystans: ";
                cin >> distance;

                scene.Move(angle, distance);
                break;
            }
            case 'o': {
                double angle = 0;
                cout << "Podaj wartosc kata obrotu w stopniach" << endl << "Wartosc kata: ";
                cin >> angle;
                scene.Rotation(angle);
                break;
            }

        }
    }
    return 1;

}
