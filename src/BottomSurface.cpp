#include "BottomSurface.hh"
#include <fstream>
using namespace std;

BottomSurface::BottomSurface() {
    ifstream inputFile;
    inputFile.open(kBottomModel);
    if (!inputFile.is_open()) {
        cerr << "Unable to load bottom surface file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        BottomSurfacePoints.push_back(point);
    }
    inputFile.close();
}

void BottomSurface::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to bottom surface file!" << endl;
        return;
    }
    for (unsigned i = 0; i < BottomSurfacePoints.size(); ++i) {
        outputFile << BottomSurfacePoints[i]<< endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}
