#include "cuboid.hh"
#include <fstream>
#include <iostream>
#include <thread>


using namespace std;

Cuboid::Cuboid() {
    ifstream inputFile;
    inputFile.open(kModelCuboid);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model Cuboid file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        points.push_back(point);
    }
    inputFile.close();
}

void Cuboid::draw(const std::string &filename) const {
    ofstream outputFile;
    RotateMatrix m;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to open drone file!" << endl;
        return;
    }
    for (unsigned i = 0; i < points.size(); ++i) {
        outputFile <<  m.RotateZ(angle) * points[i] + translation << endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}

