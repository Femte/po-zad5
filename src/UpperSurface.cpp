#include "UpperSurface.hh"
#include <fstream>

using namespace std;

UpperSurface::UpperSurface() {
    ifstream inputFile;
    inputFile.open(kUpperModel);
    if (!inputFile.is_open()) {
        cerr << "Unable to load upper surface file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        UpperSurfacePoints.push_back(point);
    }
    inputFile.close();
}

void UpperSurface::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to bottom surface file!" << endl;
        return;
    }
    for (unsigned i = 0; i < UpperSurfacePoints.size(); ++i) {
        outputFile << UpperSurfacePoints[i]<< endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}
