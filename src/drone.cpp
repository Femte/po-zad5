#include "drone.hh"
#include <fstream>

using namespace std;

void drone::Rotate(double AngleChange, const int FPS) {
    AngleChange /= FPS;
    Cuboid::AddAngleToFile(AngleChange);
    draw(kDroneFile);
}

void drone::draw(const std::string &cuboid) const {
    Cuboid::draw(cuboid);
    RotateMatrix n;
}

Vector3D drone::TranslateForward(const double &MovingAngle, const double &lenght, const int FPS) {
    Cuboid::translate(CalculatingMove(MovingAngle, lenght) / FPS);
    draw(kDroneFile);
    return shape::translation;
}

Vector3D drone::CalculatingMove(double MoveAngle, double length) const {
    Vector3D result;
    while (MoveAngle > 360)
        MoveAngle = MoveAngle - 360;
    result[0] = cos(DegreeToRadians(MoveAngle)) * cos(DegreeToRadians(angle)) * length;
    result[1] = cos(DegreeToRadians(MoveAngle)) * sin(DegreeToRadians(angle)) * length;
    result[2] = sin(DegreeToRadians(MoveAngle)) * length;

    return result;
}
