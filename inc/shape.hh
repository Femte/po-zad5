#pragma once

#include "Vector.hh"
#include "Matrix.hh"

#define PI 3.14159265

class shape {

protected:
    Vector3D translation;
    double angle;
public:

    double DegreeToRadians(double degree) const { return PI * degree / 100; }

    void AddAngleToFile(double &InputAngle) {
        angle += InputAngle;
    }

    shape() : angle{0} {};

    virtual ~shape() = default;

    void translate(const Vector3D &change) {
        translation = translation + change;
    }

    virtual void draw(const std::string &filename) const = 0;

};
