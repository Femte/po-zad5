#pragma once

#include "drone.hh"
#include "gnuplot_link.hh"
#include "fstream"
#include "string"
#include "BottomSurface.hh"
#include "UpperSurface.hh"

class scene : protected drone,BottomSurface,UpperSurface{
private:

    PzG::GnuplotLink link;

    drone drone;

    BottomSurface bottomSurface;

    UpperSurface upperSurface;

    const int FPS = 30;

    const int FREEZE = 16000;
public:
    scene();

    ~scene() override = default;

    void Rotation(double RotateAngle);

    void Move(const double &MovingAngle, const double &lenght);

};
