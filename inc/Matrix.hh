#pragma once

#include "Vector.hh"

using std::abs;

template<typename T, int SIZE>
class Matrix {
public:
    Vector<T, SIZE> row[SIZE];



    Matrix();

    const Vector<T, SIZE> &operator[](int column) const;

    Vector<T, SIZE> &operator[](int column);

    Vector<T, SIZE> operator*(Vector<T, SIZE> v); //mnozenie macierz razy wektor

    void transposed();

    Matrix<T, SIZE> operator*(T n) const; //mnozenie macierzy przez liczbe


};



template<typename T, int SIZE>
Vector<T, SIZE> &Matrix<T, SIZE>::operator[](int column) {
    assert(column < SIZE);
    return row[column];
}

template<typename T, int SIZE>
const Vector<T, SIZE> &Matrix<T, SIZE>::operator[](int column) const {
    assert(column < SIZE);
    return row[column];
}

//funkcja transponowania macierzy
template<typename T, int SIZE>
void Matrix<T, SIZE>::transposed() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            std::swap(row[i][j], row[j][i]);
        }
    }
}

template<typename T, int SIZE>
 Vector<T, SIZE> Matrix<T, SIZE>::operator*(Vector<T, SIZE> v) {
    Vector<T, SIZE> result;
    for(int j = 0 ; j < SIZE ; j++) {
        for (int i = 0; i < SIZE; ++i) {
            result[j] = result[j] + (row[j][i] * v[i]);
        }
    }
    return result;
}


template<typename T, int SIZE>
Matrix<T, SIZE> Matrix<T, SIZE>::operator*(T n) const  {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            row[i][j] = row[i][j] * n;
        }
    }
    return row;
}

template<typename T, int SIZE>
Matrix<T, SIZE>::Matrix() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            row[i][j] = 0;
        }
    }

}

template<typename T, int SIZE>
std::ostream &operator<<(std::ostream &os, const Matrix<T, SIZE> &Mac) {
    for (int j = 0; j < SIZE; j++) {
        for (int i = 0; i < SIZE; i++) {
            os << Mac[j][i] << "  ";
        }
        os << "\n";
    }
    return os;
}

template<typename T, int SIZE>
std::istream &operator>>(std::istream &is, Matrix<T, SIZE> &matrix) {
    while (isspace(is.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        is.ignore();
    }
    for (int i = 0; i < SIZE; i++) {
        is >> matrix[i];
    }
    return is;
}

