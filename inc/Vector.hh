#pragma once

#include <iostream>
#include <cassert>
#include <cmath>

template<typename T, int SIZE>
class Vector {
public:
    T data[SIZE];

    Vector();

    Vector<T, SIZE> operator+(const Vector<T, SIZE> &v2) const //dodawanie dwoch wektorow
    {
        Vector<T, SIZE> result;
        for (int i = 0; i < SIZE; i++) {
            result[i] = data[i] + v2[i];
        }
        return result;
    }

    Vector<T, SIZE> operator-(Vector<T, SIZE> &v2) const //odejmowanie dwoch dowonych takich samych wektorow
    {
        Vector<T, SIZE> result;
        for (int i = 0; i < SIZE; i++) {
            result[i] = data[i] - v2[i];
        }
        return result;
    }

    Vector<T, SIZE> operator*(T n) const { //wektor razy liczba
        Vector<T, SIZE> wynik;
        for (int i = 0; i < SIZE; i++) {
            wynik[i] = data[i] * n;
        }
        return wynik;
    }

    Vector<T, SIZE> operator/(T n) const { //wektor przez liczbe
        Vector<T, SIZE> result;
        for (int i = 0; i < SIZE; i++) {
            result[i] = data[i] / n;
        }
        return result;
    }

    T operator*(Vector<T, SIZE> &v2) const //iloczyn skalarny
    {
        T result = 0.0;
        for (int i = 0; i < SIZE; i++) {
            result = result + data[i] * v2[i];
        }
        return result;
    }

    const T &operator[](int index) const {
        assert(index < SIZE);
        return data[index];
    }

    T &operator[](int index) {
        assert(index < SIZE);
        return data[index];
    }


};

using Vector3D = Vector<double, 3>;

//konstruktor wektora,zerowanie go
template<typename T, int SIZE>
Vector<T, SIZE>::Vector() {
    for (int i = 0; i < SIZE; i++) {
        data[i] = 0;
    }
}

/**
 * Ponizej znajduja sie przeciazenia operatorow << i >>
 * sluza one kolejno do wyswietlania oraz wczytywania
 * elementow wektora
 */
template<typename T, int SIZE>
std::istream &operator>>(std::istream &is, Vector<T, SIZE> &vec) {
    while (isspace(is.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        is.ignore();
    }

    for (int i = 0; i < SIZE; i++) {
        is >> vec[i];
        if (is.get() != ' ' && (i < SIZE - 1)) {
            is.unget();
            is.setstate(std::ios::failbit);
            return is;
        }
    }
    return is;
}

template<typename T, int SIZE>
std::ostream &operator<<(std::ostream &os, const Vector<T, SIZE> &vec) {
    for (int i = 0; i < SIZE; i++) {
        os << vec[i] << " ";
    }
    return os;
}
