#pragma once

#include <vector>
#include <string>
#include "Matrix.hh"
#include "RotateMatrix.hh"
#include "shape.hh"

class Cuboid : public shape {
protected:
    std::vector<Vector3D> points;
    const std::string kModelCuboid = "solid/model.dat";

public:
    Cuboid();

    ~Cuboid() override = default;

    void draw(const std::string &filename) const override;

};


