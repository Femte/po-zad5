#pragma once
#include <vector>
#include "shape.hh"
#include <fstream>



class UpperSurface : public shape {
protected:

    const std::string kUpperModel = "solid/UpperSurfaceModel.dat";

    std::vector<Vector3D> UpperSurfacePoints;
public:
    UpperSurface();

    ~UpperSurface() override = default;

    const std::string kUpperSurface = "solid/UpperSurface.dat";

    void draw(const std::string &filename) const override;

    double UpperSurfaceLevel = 400;

};
