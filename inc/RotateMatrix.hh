#pragma once

#include "Matrix.hh"
#include <cmath>

#define PI 3.14159265

using Matrix3D = Matrix<double, 3>;

class RotateMatrix {
public:

    Matrix3D RotateZ(double angle) {
        Matrix3D matrix;
        double param = angle * PI / 180;
        matrix[0][0] = cos(param);
        matrix[0][1] = -sin(param);
        matrix[0][2] = 0;
        matrix[1][0] = sin(param);
        matrix[1][1] = cos(param);
        matrix[1][2] = 0;
        matrix[2][0] = 0;
        matrix[2][1] = 0;
        matrix[2][2] = 1;
        return matrix;
    }

    Matrix3D RotateY(double angle1) {
        Matrix3D matrix;
        double param = angle1 * PI / 180;
        matrix[0][0] = cos(param);
        matrix[0][1] = 0;
        matrix[0][2] = sin(param);
        matrix[1][0] = 0;
        matrix[1][1] = 1;
        matrix[1][2] = 0;
        matrix[2][0] = -sin(param);
        matrix[2][1] = 0;
        matrix[2][2] = cos(param);
        return matrix;
    }

    Matrix3D RotateX(double angle2) {
        Matrix3D matrix;
        double param = angle2 * PI / 180;
        matrix[0][0] = 1;
        matrix[0][1] = 0;
        matrix[0][2] = 1;
        matrix[1][0] = 0;
        matrix[1][1] = cos(param);
        matrix[1][2] = -sin(param);
        matrix[2][0] = 0;
        matrix[2][1] = sin(param);
        matrix[2][2] = cos(param);
        return matrix;
    }


};



