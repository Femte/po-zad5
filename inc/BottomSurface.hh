#pragma once
#include <vector>
#include "shape.hh"
#include <fstream>



class BottomSurface : public shape {
protected:

    const std::string kBottomModel = "solid/BottomSurfaceModel.dat";

    std::vector<Vector3D> BottomSurfacePoints;
public:
    BottomSurface();

    ~BottomSurface() override = default;

    const std::string kBottomSurface = "solid/BottomSurface.dat";

    void draw(const std::string &filename) const override;

    double BottomSurfaceLevel = 0;

};
