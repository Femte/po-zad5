#pragma once

#include "cuboid.hh"

class drone : protected Cuboid {
private:

protected:
    Vector3D CalculatingMove(double MoveAngle, double length) const;

public:
    const std::string kDroneFile = "solid/drone.dat";

    void Rotate(double AngleChange, int FPS);

    Vector3D TranslateForward(const double &MovingAngle, const double &lenght, int FPS);

    ~drone() override = default;

    void draw(const std::string &cuboid) const;

};
